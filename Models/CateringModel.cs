using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace kokkentyren
{
    public class CateringModel
    {
        [Required]
        public string eventid { get; set; }
        [Required]
        
        public int normalmeal { get; set; }
        [Required]
        public int specialmeal { get; set; }
        [Required]
        public List<string> type { get; set; }
        [Required]
        public string startdate { get; set; }
        [Required]
        public string enddate { get; set; }
        public string request_type { get; set; }


    }
}