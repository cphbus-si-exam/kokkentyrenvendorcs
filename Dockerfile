FROM mcr.microsoft.com/dotnet/core/sdk:3.0 as base
WORKDIR /src

COPY kokkentyren.csproj .
RUN dotnet restore

COPY . .
RUN dotnet publish -c Release -o out

FROM mcr.microsoft.com/dotnet/core/aspnet:3.0 as final
WORKDIR /app

COPY --from=base /src/out .
ENTRYPOINT [ "dotnet", "kokkentyren.dll" ]