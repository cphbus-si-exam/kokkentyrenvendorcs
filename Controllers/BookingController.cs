using System.Net;
using Microsoft.AspNetCore.Mvc;

namespace kokkentyren
{
    [ApiController]
    [Route("[controller]")]
    public class BookingController : Controller
    {
        [HttpGet]
        public IActionResult Get(){
            return Ok("Catering is Alive");
        }

        [HttpPost]
        public JsonResult Post([FromBody]CateringModel cateringData){

            if(!ModelState.IsValid){
                return Json("Invalid model", HttpStatusCode.BadRequest);
            }
            
            if (cateringData.normalmeal<2 && cateringData.specialmeal>10){
                var specielmodel = new CateringResModel(){status=false, description="We can not fullfill this meal distribution", eventid=cateringData.eventid, request_type=cateringData.request_type};
                return Json(specielmodel);
            }
            
            var resmodel = new CateringResModel();
            resmodel.eventid = cateringData.eventid;
            resmodel.request_type = cateringData.request_type;
            resmodel.additional_info = "";
            if (cateringData.type.Contains("morning"))
                resmodel.additional_info += "Morning meal: Buffet ";    

            if (cateringData.type.Contains("noon"))
                resmodel.additional_info += "Noon meal: fish, soup, and fruite ";    

            if (cateringData.type.Contains("dinner"))
                resmodel.additional_info += "Dinner meal: Steak, with springrolls and duck ";    

            resmodel.additional_info += "- Contact Person: Catherine Deneuve ";

            resmodel.status=true;
            return Json(resmodel);
        }
    }
}